
import java.util.HashMap;
import java.util.Objects;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Time {
    private HashMap<Integer,Jogador> jogadores;
    private String nomeCompleto, nomePopular;
    private Estadio estadioPrincipal;
    private String municipio, estadoFederacao;
    private Treinador treinador;

    public Time(String nomeCompleto, String nomePopular, Estadio estadioPrincipal, String municipio, String estadoFederacao, Treinador treinador) {
        this.nomeCompleto = nomeCompleto;
        this.nomePopular = nomePopular;
        this.estadioPrincipal = estadioPrincipal;
        this.municipio = municipio;
        this.estadoFederacao = estadoFederacao;
        this.treinador = treinador;
        this.jogadores = new HashMap<>();
        
    }
    // JOGADORES
    
    public void cadastrarJogador(Integer numero, Jogador jogador) {
        this.getJogadores().put(numero, jogador);
        
    }
    
    public void removerJogador(Integer numero) throws Exception {
        if(this.getJogadores().containsKey(numero)) {
            this.getJogadores().remove(numero);
        }
        else
            throw new Exception("Jogador não existe!");
    }
    
    // ENCAPSULAMENTO
    
    public HashMap<Integer, Jogador> getJogadores() {
        return jogadores;
    }

    public void setJogadores(HashMap<Integer, Jogador> jogadores) {
        this.jogadores = jogadores;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getNomePopular() {
        return nomePopular;
    }

    public void setNomePopular(String nomePopular) {
        this.nomePopular = nomePopular;
    }

    public Estadio getEstadioPrincipal() {
        return estadioPrincipal;
    }

    public void setEstadioPrincipal(Estadio estadioPrincipal) {
        this.estadioPrincipal = estadioPrincipal;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstadoFederacao() {
        return estadoFederacao;
    }

    public void setEstadoFederacao(String estadoFederacao) {
        this.estadoFederacao = estadoFederacao;
    }

    public Treinador getTreinador() {
        return treinador;
    }

    public void setTreinador(Treinador treinador) {
        this.treinador = treinador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.nomeCompleto);
        hash = 97 * hash + Objects.hashCode(this.nomePopular);
        hash = 97 * hash + Objects.hashCode(this.estadioPrincipal);
        hash = 97 * hash + Objects.hashCode(this.municipio);
        hash = 97 * hash + Objects.hashCode(this.estadoFederacao);
        hash = 97 * hash + Objects.hashCode(this.treinador);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Time other = (Time) obj;
        if (!Objects.equals(this.nomeCompleto, other.nomeCompleto)) {
            return false;
        }
        if (!Objects.equals(this.nomePopular, other.nomePopular)) {
            return false;
        }
        if (!Objects.equals(this.estadioPrincipal, other.estadioPrincipal)) {
            return false;
        }
        if (!Objects.equals(this.municipio, other.municipio)) {
            return false;
        }
        if (!Objects.equals(this.estadoFederacao, other.estadoFederacao)) {
            return false;
        }
        if (!Objects.equals(this.treinador, other.treinador)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Time: " + getNomeCompleto();
    }
    
}
