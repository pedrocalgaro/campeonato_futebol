
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Partida {
    
    private Time timeVisitante, timeDaCasa;
    private Collection<Gol> golsTimeVisitante, golsTimeDaCasa;
    private ArbitroPrincipal arbitro1;
    private ArbitroBandeirinha arbitro2;
    private ArbitroBandeirinha arbitro3;
    private ArbitroPrincipal arbitro4;
    private Date dataPartida;
    private String horarioPartida;
    private Estadio localPartida;
    private Integer numeroDePagantes, numeroDeNaoPagantes;
    private Double rendaTotal;

    public Partida(Time timeVisitante, Time timeDaCasa, ArbitroPrincipal arbitro1, ArbitroBandeirinha arbitro2, ArbitroBandeirinha arbitro3, ArbitroPrincipal arbitro4, Date dataPartida, String horarioPartida, Estadio localPartida, Integer numeroDePagantes, Integer numeroDeNaoPagantes, Double rendaTotal) {
        this.timeVisitante = timeVisitante;
        this.timeDaCasa = timeDaCasa;
        this.arbitro1 = arbitro1;
        this.arbitro2 = arbitro2;
        this.arbitro3 = arbitro3;
        this.arbitro4 = arbitro4;
        this.dataPartida = dataPartida;
        this.horarioPartida = horarioPartida;
        this.localPartida = localPartida;
        this.numeroDePagantes = numeroDePagantes;
        this.numeroDeNaoPagantes = numeroDeNaoPagantes;
        this.rendaTotal = rendaTotal;
        Random r = new Random();
        int qtdGol = r.nextInt(10);
        this.golsTimeDaCasa = new LinkedList<>();
        for (int i = 0; i < qtdGol; i++) {
            Gol gol = new Gol();
            golsTimeDaCasa.add(gol);
        }
        this.golsTimeVisitante = new LinkedList<>();
        qtdGol = r.nextInt(10);
        for (int i = 0; i < qtdGol; i++) {
            Gol gol = new Gol();
            golsTimeVisitante.add(gol);
        }
    }
    
    
    // GOLS TIME DA CASA
    
    public void cadastrarGolTimeCasa(Gol gol) throws Exception {
        if(this.getGolsTimeDaCasa().contains(gol)) {
            throw new Exception("Gol já existe!");
        }
        else
            this.getGolsTimeDaCasa().add(gol);
    }
    
    public void removerGolTimeCasa(Gol gol) throws Exception {
        if(this.getGolsTimeDaCasa().contains(gol)) {
            this.getGolsTimeDaCasa().remove(gol);
        }
        else
            throw new Exception("Gol não existe!");
    }
    
    // GOLS TIME VISITANTE
    
    public void cadastrarGolTimeVisitante(Gol gol) throws Exception {
        if(this.getGolsTimeVisitante().contains(gol)) {
            throw new Exception("Gol já existe!");
        }
        else
            this.getGolsTimeVisitante().add(gol);
    }
    
    public void removerGolTimeVisitante(Gol gol) throws Exception {
        if(this.getGolsTimeVisitante().contains(gol)) {
            this.getGolsTimeVisitante().remove(gol);
        }
        else
            throw new Exception("Gol não existe!");
    }
    
    public Time getTimeVisitante() {
        return timeVisitante;
    }

    public void setTimeVisitante(Time timeVisitante) {
        this.timeVisitante = timeVisitante;
    }

    public Time getTimeDaCasa() {
        return timeDaCasa;
    }

    public void setTimeDaCasa(Time timeDaCasa) {
        this.timeDaCasa = timeDaCasa;
    }

    public Collection<Gol> getGolsTimeVisitante() {
        return golsTimeVisitante;
    }

    public void setGolsTimeVisitante(Collection<Gol> golsTimeVisitante) {
        this.golsTimeVisitante = golsTimeVisitante;
    }

    public Collection<Gol> getGolsTimeDaCasa() {
        return golsTimeDaCasa;
    }

    public void setGolsTimeDaCasa(Collection<Gol> golsTimeDaCasa) {
        this.golsTimeDaCasa = golsTimeDaCasa;
    }

    public ArbitroPrincipal getArbitro1() {
        return arbitro1;
    }

    public void setArbitro1(ArbitroPrincipal arbitro1) {
        this.arbitro1 = arbitro1;
    }

    public ArbitroBandeirinha getArbitro2() {
        return arbitro2;
    }

    public void setArbitro2(ArbitroBandeirinha arbitro2) {
        this.arbitro2 = arbitro2;
    }

    public ArbitroBandeirinha getArbitro3() {
        return arbitro3;
    }

    public void setArbitro3(ArbitroBandeirinha arbitro3) {
        this.arbitro3 = arbitro3;
    }

    public ArbitroPrincipal getArbitro4() {
        return arbitro4;
    }

    public void setArbitro4(ArbitroPrincipal arbitro4) {
        this.arbitro4 = arbitro4;
    }

    public Date getDataPartida() {
        return dataPartida;
    }

    public void setDataPartida(Date dataPartida) {
        this.dataPartida = dataPartida;
    }

    public String getHorarioPartida() {
        return horarioPartida;
    }

    public void setHorarioPartida(String horarioPartida) {
        this.horarioPartida = horarioPartida;
    }

    public Estadio getLocalPartida() {
        return localPartida;
    }

    public void setLocalPartida(Estadio localPartida) {
        this.localPartida = localPartida;
    }

    public Integer getNumeroDePagantes() {
        return numeroDePagantes;
    }

    public void setNumeroDePagantes(Integer numeroDePagantes) {
        this.numeroDePagantes = numeroDePagantes;
    }

    public Integer getNumeroDeNaoPagantes() {
        return numeroDeNaoPagantes;
    }

    public void setNumeroDeNaoPagantes(Integer numeroDeNaoPagantes) {
        this.numeroDeNaoPagantes = numeroDeNaoPagantes;
    }

    public Double getRendaTotal() {
        return rendaTotal;
    }

    public void setRendaTotal(Double rendaTotal) {
        this.rendaTotal = rendaTotal;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.timeVisitante);
        hash = 17 * hash + Objects.hashCode(this.timeDaCasa);
        hash = 17 * hash + Objects.hashCode(this.arbitro1);
        hash = 17 * hash + Objects.hashCode(this.arbitro2);
        hash = 17 * hash + Objects.hashCode(this.arbitro3);
        hash = 17 * hash + Objects.hashCode(this.arbitro4);
        hash = 17 * hash + Objects.hashCode(this.dataPartida);
        hash = 17 * hash + Objects.hashCode(this.horarioPartida);
        hash = 17 * hash + Objects.hashCode(this.localPartida);
        hash = 17 * hash + Objects.hashCode(this.numeroDePagantes);
        hash = 17 * hash + Objects.hashCode(this.numeroDeNaoPagantes);
        hash = 17 * hash + Objects.hashCode(this.rendaTotal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Partida other = (Partida) obj;
        if (!Objects.equals(this.timeDaCasa, other.timeDaCasa)) {
            return false;
        }
        if (!Objects.equals(this.arbitro1, other.arbitro1)) {
            return false;
        }
        if (!Objects.equals(this.arbitro2, other.arbitro2)) {
            return false;
        }
        if (!Objects.equals(this.arbitro3, other.arbitro3)) {
            return false;
        }
        if (!Objects.equals(this.arbitro4, other.arbitro4)) {
            return false;
        }
        if (!Objects.equals(this.dataPartida, other.dataPartida)) {
            return false;
        }
        if (!Objects.equals(this.horarioPartida, other.horarioPartida)) {
            return false;
        }
        if (!Objects.equals(this.localPartida, other.localPartida)) {
            return false;
        }
        if (!Objects.equals(this.numeroDePagantes, other.numeroDePagantes)) {
            return false;
        }
        if (!Objects.equals(this.numeroDeNaoPagantes, other.numeroDeNaoPagantes)) {
            return false;
        }
        if (!Objects.equals(this.rendaTotal, other.rendaTotal)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Partida{" + "timeVisitante=" + timeVisitante + ", timeDaCasa=" + timeDaCasa + ", golsTimeVisitante=" + golsTimeVisitante + ", golsTimeDaCasa=" + golsTimeDaCasa + ", arbitro1=" + arbitro1 + ", arbitro2=" + arbitro2 + ", arbitro3=" + arbitro3 + ", arbitro4=" + arbitro4 + ", dataPartida=" + dataPartida + ", horarioPartida=" + horarioPartida + ", localPartida=" + localPartida + ", numeroDePagantes=" + numeroDePagantes + ", numeroDeNaoPagantes=" + numeroDeNaoPagantes + ", rendaTotal=" + rendaTotal + '}';
    }
    
}
