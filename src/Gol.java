
import java.util.Objects;
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Gol {
    Jogador jogador;
    String minutos;
    Integer etapaPartida;

    public Gol(Jogador jogador, String minutos, Integer etapaPartida) {
        this.jogador = jogador;
        this.minutos = minutos;
        this.etapaPartida = etapaPartida;
    }

    public Gol() {
        Random r = new Random();
        this.jogador = null;
        this.minutos = "30";
        this.etapaPartida = 1;
    }
    
    // ENCAPSULAMENTO

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public String getMinutos() {
        return minutos;
    }

    public void setMinutos(String minutos) {
        this.minutos = minutos;
    }

    public Integer getEtapaPartida() {
        return etapaPartida;
    }

    public void setEtapaPartida(Integer etapaPartida) {
        this.etapaPartida = etapaPartida;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.jogador);
        hash = 97 * hash + Objects.hashCode(this.minutos);
        hash = 97 * hash + Objects.hashCode(this.etapaPartida);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gol other = (Gol) obj;
        if (!Objects.equals(this.jogador, other.jogador)) {
            return false;
        }
        if (!Objects.equals(this.minutos, other.minutos)) {
            return false;
        }
        if (!Objects.equals(this.etapaPartida, other.etapaPartida)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Gol{" + "jogador=" + jogador + ", minutos=" + minutos + ", etapaPartida=" + etapaPartida + '}';
    }
    
}
