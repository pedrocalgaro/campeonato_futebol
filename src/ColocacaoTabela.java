/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class ColocacaoTabela {
    private Time time;
    private Integer nPontos, nVitorias, nEmpates, nDerrotas;
    private Integer nGolsMarcados, nGolsSofridos;

    public ColocacaoTabela(Integer nPontos) {
        this.nPontos = nPontos;
    }

    public ColocacaoTabela(Time time) {
        this.time = time;
        this.nPontos = 0;
        this.nVitorias = 0;
        this.nEmpates = 0;
        this.nDerrotas = 0;
        this.nGolsMarcados = 0;
        this.nGolsSofridos = 0;        
    }
   
    public ColocacaoTabela(Time time, Integer nPontos, Integer nVitorias, Integer nEmpates, Integer nDerrotas, Integer nGolsMarcados, Integer nGolsSofridos) {
        this.time = time;
        this.nPontos = nPontos;
        this.nVitorias = nVitorias;
        this.nEmpates = nEmpates;
        this.nDerrotas = nDerrotas;
        this.nGolsMarcados = nGolsMarcados;
        this.nGolsSofridos = nGolsSofridos;
    }
    
    public Integer getSaldoDeGols()
    {
        return getnGolsMarcados() - getnGolsSofridos();
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Integer getnPontos() {
        return nPontos;
    }

    public void setnPontos(Integer nPontos) {
        this.nPontos = nPontos;
    }

    public Integer getnVitorias() {
        return nVitorias;
    }

    public void setnVitorias(Integer nVitorias) {
        this.nVitorias = nVitorias;
    }

    public Integer getnEmpates() {
        return nEmpates;
    }

    public void setnEmpates(Integer nEmpates) {
        this.nEmpates = nEmpates;
    }

    public Integer getnDerrotas() {
        return nDerrotas;
    }

    public void setnDerrotas(Integer nDerrotas) {
        this.nDerrotas = nDerrotas;
    }

    public Integer getnGolsMarcados() {
        return nGolsMarcados;
    }

    public void setnGolsMarcados(Integer nGolsMarcados) {
        this.nGolsMarcados = nGolsMarcados;
    }

    public Integer getnGolsSofridos() {
        return nGolsSofridos;
    }

    public void setnGolsSofridos(Integer nGolsSofridos) {
        this.nGolsSofridos = nGolsSofridos;
    }

    @Override
    public String toString() {
        return "ColocacaoTabela{" + "time=" + time + ", nPontos=" + nPontos + ", nVitorias=" + nVitorias + ", nEmpates=" + nEmpates + ", nDerrotas=" + nDerrotas + ", nGolsMarcados=" + nGolsMarcados + ", nGolsSofridos=" + nGolsSofridos + '}';
    }
}
