/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Estadio {
    private String nomeCompletoEstadio, nomePopularEstadio;
    private String municipio, estadoFederacao;
    private Integer capacidadeMaxima;

    public Estadio(String nomeCompletoEstadio, String nomePopularEstadio, String municipio, String estadoFederacao, Integer capacidadeMaxima) {
        this.nomeCompletoEstadio = nomeCompletoEstadio;
        this.nomePopularEstadio = nomePopularEstadio;
        this.municipio = municipio;
        this.estadoFederacao = estadoFederacao;
        this.capacidadeMaxima = capacidadeMaxima;
    }

    public String getNomeCompletoEstadio() {
        return nomeCompletoEstadio;
    }

    public void setNomeCompletoEstadio(String nomeCompletoEstadio) {
        this.nomeCompletoEstadio = nomeCompletoEstadio;
    }

    public String getNomePopularEstadio() {
        return nomePopularEstadio;
    }

    public void setNomePopularEstadio(String nomePopularEstadio) {
        this.nomePopularEstadio = nomePopularEstadio;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstadoFederacao() {
        return estadoFederacao;
    }

    public void setEstadoFederacao(String estadoFederacao) {
        this.estadoFederacao = estadoFederacao;
    }

    public Integer getCapacidadeMaxima() {
        return capacidadeMaxima;
    }

    public void setCapacidadeMaxima(Integer capacidadeMaxima) {
        this.capacidadeMaxima = capacidadeMaxima;
    }

    @Override
    public String toString() {
        return "Estadio{" + "nomeCompletoEstadio=" + nomeCompletoEstadio + ", nomePopularEstadio=" + nomePopularEstadio + ", municipio=" + municipio + ", estadoFederacao=" + estadoFederacao + ", capacidadeMaxima=" + capacidadeMaxima + '}';
    }
    
}
