
import java.util.Date;
import java.util.Objects;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Jogador extends PessoaComContrato{
    private Posicao atualPosicaoPrincipal;

    public Jogador(String nomeCompleto, String nomeReduzidoPopular, Date dataNascimento, Posicao atualPosicaoPrincipal, Contrato contrato) {
        super(nomeCompleto, nomeReduzidoPopular, dataNascimento, contrato);
        this.atualPosicaoPrincipal = atualPosicaoPrincipal;
    }
    
    public Posicao getAtualPosicaoPrincipal() {
        return atualPosicaoPrincipal;
    }

    public void setAtualPosicaoPrincipal(Posicao atualPosicaoPrincipal) {
        this.atualPosicaoPrincipal = atualPosicaoPrincipal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.atualPosicaoPrincipal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jogador other = (Jogador) obj;
        if (this.atualPosicaoPrincipal != other.atualPosicaoPrincipal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Jogador{" + "atualPosicaoPrincipal=" + atualPosicaoPrincipal + '}';
    }
    
}
