
import java.util.Date;
import java.util.Objects;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Arbitro extends Pessoa{

    protected String federacao;
    protected Boolean fifa;
    
    public Arbitro(String nomeCompleto, String nomeReduzidoPopular, Date dataNascimento) {
        super(nomeCompleto, nomeReduzidoPopular, dataNascimento);
    }
    
    public String getFederacao() {
        return federacao;
    }

    public void setFederacao(String federacao) {
        this.federacao = federacao;
    }

    public Boolean isFifa() {
        return fifa;
    }

    public void setFifa(Boolean fifa) {
        this.fifa = fifa;
    }

    @Override
    public String getNomeCompleto() {
        return nomeCompleto;
    }

    @Override
    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    @Override
    public String getNomeReduzidoPopular() {
        return nomeReduzidoPopular;
    }

    @Override
    public void setNomeReduzidoPopular(String nomeReduzidoPopular) {
        this.nomeReduzidoPopular = nomeReduzidoPopular;
    }

    @Override
    public Date getDataNascimento() {
        return dataNascimento;
    }

    @Override
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.federacao);
        hash = 59 * hash + Objects.hashCode(this.fifa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Arbitro other = (Arbitro) obj;
        if (!Objects.equals(this.federacao, other.federacao)) {
            return false;
        }
        if (!Objects.equals(this.fifa, other.fifa)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Arbitro{" + "federacao=" + federacao + ", fifa=" + fifa + '}';
    }
}
