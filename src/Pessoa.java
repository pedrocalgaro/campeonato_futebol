
import java.util.Comparator;
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Pessoa {
    protected String nomeCompleto, nomeReduzidoPopular;
    protected Date dataNascimento;

    public Pessoa(String nomeCompleto, String nomeReduzidoPopular, Date dataNascimento) {
        this.nomeCompleto = nomeCompleto;
        this.nomeReduzidoPopular = nomeReduzidoPopular;
        this.dataNascimento = dataNascimento;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getNomeReduzidoPopular() {
        return nomeReduzidoPopular;
    }

    public void setNomeReduzidoPopular(String nomeReduzidoPopular) {
        this.nomeReduzidoPopular = nomeReduzidoPopular;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nomeCompleto=" + nomeCompleto + ", nomeReduzidoPopular=" + nomeReduzidoPopular + ", dataNascimento=" + dataNascimento + '}';
    }
    
        
    public static Comparator<Pessoa> getComparatorNomeCompleto() {
        return new Comparator<Pessoa>() {

            @Override
            public int compare(Pessoa o1, Pessoa o2) {
                return o1.nomeCompleto.compareTo(o2.nomeCompleto);
            }
        };
    }    
    
    public static Comparator<Pessoa> getComparatorNomeReduzidoPopular() {
        return new Comparator<Pessoa>() {

            @Override
            public int compare(Pessoa o1, Pessoa o2) {
                return o1.nomeReduzidoPopular.compareTo(o2.nomeReduzidoPopular);
            }
        };
    }    
    
}
