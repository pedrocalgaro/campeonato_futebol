
import java.text.SimpleDateFormat;
import java.util.*;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Hercules Sandim
 */
public class Campeonato {

    private String nomeCampeonato;
    private Collection<Time> timesDoCampeonato;
    private Collection<Arbitro> arbitrosCredenciados;
    private Collection<Rodada> rodadasDoCampeonato;

    public Campeonato(String nomeCampeonato) {
        this.nomeCampeonato = nomeCampeonato;
        this.timesDoCampeonato = new ArrayList<>();
        this.arbitrosCredenciados = new LinkedList<>();
        this.rodadasDoCampeonato = new ArrayList<>();
    }

    // TIMES
    public void cadastrarTime(Time time) throws Exception {
        if (this.getTimesDoCampeonato().contains(time)) {
            throw new Exception("Time já existe!");
        }
        else {
            this.getTimesDoCampeonato().add(time);
        }
    }

    public void removerTime(Time time) throws Exception {
        if (this.getTimesDoCampeonato().contains(time)) {
            this.getTimesDoCampeonato().remove(time);
        }
        else {
            throw new Exception("Time não existe!");
        }
    }

    // ARBITROS
    public void cadastrarArbitro(Arbitro arbitro) throws Exception {
        if (this.getArbitrosCredenciados().contains(arbitro)) {
            throw new Exception("Time já existe!");
        }
        else {
            this.getArbitrosCredenciados().add(arbitro);
        }
    }

    public void removerArbitro(Arbitro arbitro) throws Exception {
        if (this.getArbitrosCredenciados().contains(arbitro)) {
            throw new Exception("Arbitro não existe!");
        }
        else {
            this.getArbitrosCredenciados().remove(arbitro);
        }
    }

    // RODADAS
    public void cadastrarRodada(Rodada rodada) throws Exception {
        if (this.getRodadasDoCampeonato().contains(rodada)) {
            throw new Exception("Rodada já existe!");
        }
        else {
            this.getRodadasDoCampeonato().add(rodada);
        }
    }

    public void removerRodada(Rodada rodada) throws Exception {
        if (this.getRodadasDoCampeonato().contains(rodada)) {
            throw new Exception("Arbitro não existe!");
        }
        else {
            this.getRodadasDoCampeonato().remove(rodada);
        }
    }

    // ENCAPSULAMENTO
    public String getNomeCampeonato() {
        return nomeCampeonato;
    }

    public void setNomeCampeonato(String nomeCampeonato) {
        this.nomeCampeonato = nomeCampeonato;
    }

    public Collection<Time> getTimesDoCampeonato() {
        return timesDoCampeonato;
    }

    public void setTimesDoCampeonato(Collection<Time> timesDoCampeonato) {
        this.timesDoCampeonato = timesDoCampeonato;
    }

    public Collection<Arbitro> getArbitrosCredenciados() {
        return arbitrosCredenciados;
    }

    public void setArbitrosCredenciados(Collection<Arbitro> arbitrosCredenciados) {
        this.arbitrosCredenciados = arbitrosCredenciados;
    }

    public Collection<Rodada> getRodadasDoCampeonato() {
        return rodadasDoCampeonato;
    }

    public void setRodadasDoCampeonato(Collection<Rodada> rodadasDoCampeonato) {
        this.rodadasDoCampeonato = rodadasDoCampeonato;
    }

    public HashMap<Integer, ColocacaoTabela> tabelaClassificacao() {
        //PARA IMPLEMENTAR
        HashMap<Integer, ColocacaoTabela> tabelaClassificacao = new HashMap<>();
        
        ArrayList<ColocacaoTabela> colocacoes = new ArrayList<>();
        for (Time time : timesDoCampeonato) {
            ColocacaoTabela colocacaoTabelaDoTime = new ColocacaoTabela(time);
            colocacaoTabelaDoTime.setTime(time);
            for (Rodada rodada : rodadasDoCampeonato) {
                for (Partida partida : rodada.getPartidas()) {
                    Time timeDaCasa = partida.getTimeDaCasa();
                    Time timeVisitante = partida.getTimeVisitante();
                    /*Essa condição verifica se o time da casa ou time visitante é igual ao time
                     que está sendo procurado*/
                    if (time.equals(timeDaCasa) || time.equals(timeVisitante)) {
                        //se ocorreu entre os times
                        if (partida.getGolsTimeDaCasa().size() == partida.getGolsTimeVisitante().size()) {
                            colocacaoTabelaDoTime.setnEmpates(colocacaoTabelaDoTime.getnEmpates() + 1);
                            colocacaoTabelaDoTime.setnPontos(colocacaoTabelaDoTime.getnPontos() + 1);
                        }
                        //se o time buscado é o time da casa, as informações são atualizadas na colocação do time                        
                        else if (time.equals(timeDaCasa)) {
                            //atualiza gols marcados e sofridos
                            colocacaoTabelaDoTime.setnGolsMarcados(colocacaoTabelaDoTime.getnGolsMarcados() + partida.getGolsTimeDaCasa().size());
                            colocacaoTabelaDoTime.setnGolsSofridos(colocacaoTabelaDoTime.getnGolsSofridos() + partida.getGolsTimeVisitante().size());
                            /*verificar se o time venceu, perdeu ou empatou na partida*/
                            //time da casa venceu
                            if (partida.getGolsTimeDaCasa().size() > partida.getGolsTimeVisitante().size()) {
                                colocacaoTabelaDoTime.setnVitorias(colocacaoTabelaDoTime.getnVitorias() + 1);
                                colocacaoTabelaDoTime.setnPontos(colocacaoTabelaDoTime.getnPontos() + 3);
                            }
                            //time da casa perdeu
                            else {
                                colocacaoTabelaDoTime.setnDerrotas(colocacaoTabelaDoTime.getnDerrotas() + 1);
                            }
                        }
                        //se o time buscado é o time visitante, as informações são atualizadas na colocação do time
                        else {
                            colocacaoTabelaDoTime.setnGolsMarcados(colocacaoTabelaDoTime.getnGolsMarcados() + partida.getGolsTimeVisitante().size());
                            colocacaoTabelaDoTime.setnGolsSofridos(colocacaoTabelaDoTime.getnGolsSofridos() + partida.getGolsTimeDaCasa().size());
                            /*verificar se o time venceu, perdeu ou empatou na partida*/

                            //time visitante venceu
                            if (partida.getGolsTimeVisitante().size() > partida.getGolsTimeDaCasa().size()) {
                                colocacaoTabelaDoTime.setnVitorias(colocacaoTabelaDoTime.getnVitorias() + 1);
                                colocacaoTabelaDoTime.setnPontos(colocacaoTabelaDoTime.getnPontos() + 3);
                            }
                            //time visitante perdeu
                            else {
                                colocacaoTabelaDoTime.setnDerrotas(colocacaoTabelaDoTime.getnDerrotas() + 1);
                            }
                        }
                    }
                }
            }

            colocacoes.add(colocacaoTabelaDoTime);
        }
        Collections.sort(colocacoes, Campeonato.getComparatorColocacoes());

        int i = 1;
        for (ColocacaoTabela colocacaoTabela : colocacoes) {
            tabelaClassificacao.put(i, colocacaoTabela);
            i++;
        }
        return tabelaClassificacao;
    }

    public void inicializarRodadas() throws Exception {

        rodadasDoCampeonato.clear();
        int quantidadeRodadas = (this.getTimesDoCampeonato().size() - 1) * 2;
        SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yyyy");
        ArrayList<Time> arrayDeTimes = new ArrayList<>();
        arrayDeTimes.addAll(timesDoCampeonato);
        Collections.shuffle(arrayDeTimes);
        Date dateIda = new Date("2013/02/01");
        Date dateVolta = new Date(dateIda.getDate() + (timesDoCampeonato.size() / 2 - 1));

        //preenchendo matriz de geração de rodadas com os times

        Time[][] matrizTimes = new Time[2][arrayDeTimes.size() / 2];

        int proximoTime = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < arrayDeTimes.size() / 2; j++) {
                Time time = arrayDeTimes.get(proximoTime);
                matrizTimes[i][j] = time;
                proximoTime++;
            }
        }

        //System.out.println("Array de times => " + arrayDeTimes);
        ArrayList<Rodada> rodadasVolta = new ArrayList<>();

        //Aqui é gerado as partidas e as rodadas
        for (int m = 0; m < arrayDeTimes.size() - 1; m++) {
            boolean gerarRodadas = true, primeiraLinha = false;
            Time aux = null, aux2 = null;
            dateIda.setDate(dateIda.getDate() + 7);
            dateVolta.setDate(dateVolta.getDate() + 7);
            Rodada novaRodadaIda = new Rodada((m + 1), dateIda, dateIda, false);
            Rodada novaRodadaVolta = new Rodada((arrayDeTimes.size() + m), dateVolta, dateVolta, false);

            for (int j = 0; j < arrayDeTimes.size() / 2; j++) {
                Partida novaPartida = new Partida(matrizTimes[0][j], matrizTimes[1][j], null, null, null, null, dateIda, "21:00", null, 10000, 100, 300000.00);
                novaRodadaIda.cadastrarPartida(novaPartida);
                novaPartida = new Partida(matrizTimes[1][j], matrizTimes[0][j], null, null, null, null, dateVolta, "21:00", null, 10000, 100, 300000.00);
                novaRodadaVolta.cadastrarPartida(novaPartida);
            }

            getRodadasDoCampeonato().add(novaRodadaIda);
            rodadasVolta.add(novaRodadaVolta);
            if (timesDoCampeonato.size() > 2) {
                /*Rotação da matriz de times para geração de novas partidas e rodadas
                 * Esse algoritmo só é utilizado quando tem mais de dois times no campeonato
                 */
                int i = 0, k = 0;
                do {
                    //if especifico para a primeira linha da matriz
                    if (primeiraLinha == false) {
                        if (i == 0) {
                            aux = matrizTimes[0][1];
                            matrizTimes[0][1] = matrizTimes[1][0];
                            i++;
                        } // ultimo elemento da primeira linha da matriz
                        else if (i == (arrayDeTimes.size() / 2 - 1)) {
                            k++;
                            aux2 = matrizTimes[k][i];
                            matrizTimes[k][i] = aux;
                            aux = aux2;
                            primeiraLinha = true;
                            i--;
                        }
                        //caso onde a unica operação a se fazer é escrever o elemento na próxima posição e remover guardar o antigo
                        else {
                            aux2 = matrizTimes[k][i + 1];
                            matrizTimes[k][i + 1] = aux;
                            aux = aux2;
                            i++;
                        }
                    }
                    //percorrendo a segunda linha da matriz
                    else {
                        aux2 = matrizTimes[k][i];
                        matrizTimes[k][i] = aux;
                        aux = aux2;
                        if (i == 0) {
                            gerarRodadas = false;
                        }
                        i--;
                    }
                } while (gerarRodadas);
            }
        }

        rodadasDoCampeonato.addAll(rodadasVolta);
        String info = "", title = "";
        title = title + "Rodadas do " + nomeCampeonato;
        for (Rodada rodada : rodadasDoCampeonato) {
            info = info + ("Rodada " + rodada.getNumeroDaRodada() + ":");
            for (Partida partidas : rodada.getPartidas()) {
                info = info + "    " + partidas.getTimeDaCasa().getNomeCompleto() + " vs " + partidas.getTimeVisitante().getNomeCompleto();
            }
            info = info + "\n";
        }
        CampeonatoApp.mostraMensagem(info, title);
    }

    @Override
    public String toString() {
        return "Campeonato{" + "nomeCampeonato=" + nomeCampeonato + ", timesDoCampeonato=" + timesDoCampeonato + ", arbitrosCredenciados=" + arbitrosCredenciados + ", rodadasDoCampeonato=" + rodadasDoCampeonato + '}';
    }

    public static Comparator<ColocacaoTabela> getComparatorColocacoes() {
        return new Comparator<ColocacaoTabela>() {
            int valor;

            @Override
            public int compare(ColocacaoTabela o1, ColocacaoTabela o2) {
                valor = o2.getnPontos().compareTo(o1.getnPontos());

                if (valor == 0) {
                    valor = o2.getnVitorias().compareTo(o1.getnVitorias());
                }
                else {
                    return valor;
                }

                if (valor == 0) {
                    valor = o2.getSaldoDeGols().compareTo(o1.getSaldoDeGols());
                }
                else {
                    return valor;
                }

                if (valor == 0) {
                    valor = o2.getnGolsMarcados().compareTo(o1.getnGolsMarcados());
                }
                else {
                    return valor;
                }

                return valor;
            }
        };
    }
}
