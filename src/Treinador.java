
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Treinador extends PessoaComContrato {
    private ArrayList<String> titulosConquistados;

    public Treinador(String nomeCompleto, String nomeReduzidoPopular, Date dataNascimento, Contrato contrato) {
        super(nomeCompleto, nomeReduzidoPopular, dataNascimento,contrato);
        titulosConquistados = new ArrayList<>();
    }
    
    // TITULOS
    
    public void cadastrarTitulo(String titulo) throws Exception {
        if(this.getTitulosConquistados().contains(titulo)) {
            throw new Exception("Titulo já existe!");
        }
        else
            this.getTitulosConquistados().add(titulo);
    }
    
    public void removerTitulo(String titulo) throws Exception {
        if(this.getTitulosConquistados().contains(titulo)) {
            this.getTitulosConquistados().remove(titulo);
        }
        else
            throw new Exception("Titulo não existe!");
    }
    
    // ENCAPSULAMENTO
    
    public Collection getTitulosConquistados() {
        return titulosConquistados;
    }

    public void setTitulosConquistados(ArrayList<String> titulosConquistados) {
        this.titulosConquistados = titulosConquistados;
    }

    @Override
    public String toString() {
        return "Treinador{" + "titulosConquistados=" + titulosConquistados + '}';
    }
    
}
