
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Rodada {
    private Integer numeroDaRodada;
    private Date dataInicio, dataTermino;
    private Collection<Partida> partidas;
    private Boolean finalizada;

    public Rodada(Integer numeroDaRodada, Date dataInicio, Date dataTermino, Boolean finalizada) {
        this.numeroDaRodada = numeroDaRodada;
        this.dataInicio = dataInicio;
        this.dataTermino = dataTermino;
        this.finalizada = finalizada;
        this.partidas = new ArrayList<>();
    }

    
    // PARTIDAS
    
    public void cadastrarPartida(Partida partida) {
        
        this.getPartidas().add(partida);
    }
    
    public void removerPartida(Partida partida) throws Exception {
        if(this.getPartidas().contains(partida)) {
            this.getPartidas().remove(partida);
        }
        else
            throw new Exception("Partida não existe!");
    }
    
    // ENCAPSULAMENTO
    
    public Integer getNumeroDaRodada() {
        return numeroDaRodada;
    }

    public void setNumeroDaRodada(Integer numeroDaRodada) {
        this.numeroDaRodada = numeroDaRodada;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(Date dataTermino) {
        this.dataTermino = dataTermino;
    }

    public Collection<Partida> getPartidas() {
        return partidas;
    }

    public void setPartidas(Collection<Partida> partidas) {
        this.partidas = partidas;
    }

    public Boolean isFinalizada() {
        return finalizada;
    }

    public void setFinalizada(Boolean finalizada) {
        this.finalizada = finalizada;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.dataInicio);
        hash = 73 * hash + Objects.hashCode(this.dataTermino);
        hash = 73 * hash + Objects.hashCode(this.finalizada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rodada other = (Rodada) obj;
        if (!Objects.equals(this.dataInicio, other.dataInicio)) {
            return false;
        }
        if (!Objects.equals(this.dataTermino, other.dataTermino)) {
            return false;
        }
        if (!Objects.equals(this.finalizada, other.finalizada)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Rodada{" + "numeroDaRodada=" + numeroDaRodada + ", dataInicio=" + dataInicio + ", dataTermino=" + dataTermino + ", partidas=" + partidas + ", finalizada=" + finalizada + '}';
    }
    
}
