
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gustavo
 */
public class CampeonatoApp {

    public static void main(String args[]) throws Exception {
        Posicao[] posicoes = Posicao.values();
        Posicao posicaotest = null;
        posicaotest = Posicao.GOLEIRO;
        System.out.println(posicaotest.getNumeroPosicao());

        /*
         Campeonato campeonatoApp = new Campeonato("Campeonato Brasileiro");
         Date date = new Date("2013/02/01");
         SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
         //System.out.println(dt1.format(date));
         //TREINADOR
         Treinador treinardorGeral = new Treinador("Muricy Ramalho", "Muricy", date, new Contrato());
         //TIMES
         Time saopaulo = new Time("Sao Paulo Futebol Clube", "SAO", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time flamengo = new Time("Flamengo Futebol Clube", "FLA", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time corinthians = new Time("Corinthians Futebol Clube", "COR", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time palmeiras = new Time("Palmeiras Futebol Clube", "PAL", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time botafogo = new Time("Botafogo Futebol Clube", "BOT", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time internacional = new Time("Internacional", "INT", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time gremio = new Time("Gremio", "GRE", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time pontepreta = new Time("Ponte Preta", "PON", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time fluminense = new Time("Fluminense", "FLU", null, "Sao Paulo", "Brasil", treinardorGeral);
         Time cruzeiro = new Time("Cruzeiro", "CRU", null, "Sao Paulo", "Brasil", treinardorGeral);

         campeonatoApp.cadastrarTime(saopaulo);
         campeonatoApp.cadastrarTime(flamengo);
         campeonatoApp.cadastrarTime(corinthians);
         campeonatoApp.cadastrarTime(palmeiras);
         campeonatoApp.cadastrarTime(botafogo);
         campeonatoApp.cadastrarTime(internacional);
         campeonatoApp.cadastrarTime(gremio);
         campeonatoApp.cadastrarTime(pontepreta);
         campeonatoApp.cadastrarTime(fluminense);
         campeonatoApp.cadastrarTime(cruzeiro);

         System.out.println("Ordem cadastro: " + campeonatoApp.getTimesDoCampeonato());


         Partida partida = new Partida(saopaulo, flamengo, null, null, null, null,
         date, null, null, 100, 100, 3000000.00);

         campeonatoApp.inicializarRodadas();


         //rodada.cadastrarPartida(partida);

         }
         */
        Campeonato campeonatoApp = new Campeonato("Campeonato Brasileiro");
        //Sao Paulo
        String nomeTime = "Sao Paulo";
        //TODO : Verificacao se o time ja existe
        String nomePopularTime = "Tricolor";
        String municipioTime = "Sao Paulo";
        String estadoTime = "Sao Paulo";
        //Estádio
        String nomeEstadioTime = "Morumbi";
        String nomePopularEstadioTime = "Morumbi";
        String municipioEstadioTime = "Sao Paulo";
        String estadoEstadioTime = "Sao Paulo";
        int capacicidadeEstadio = 100;
        Estadio estadioTime = new Estadio(nomeEstadioTime, nomePopularEstadioTime, municipioEstadioTime, estadoEstadioTime, capacicidadeEstadio);
        //Treinador
        String nomeCompletoTreinador = "Muricy Ramalho";
        String nomeReduzidoTreinador = "Muricy";
        Date dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
        //Contrato - treinador
        Double valorMultaRecisoria = 100.00;
        Double valorSalarioBase = 200.00;
        Double valorDireitosImagem = 300.00;
        Date dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        Date dataTermimo = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        String descricao = "descricao";
        //Pessoa envolvida - treinador
        String nomePessoaEnvolvida = "Pessoa envolvida";
        String nomeReduzidoPessoaEnvolvida = "Pessoa envolvida reduzido";
        Date dataNascimentoPessoaEnvolvida = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");

        Pessoa pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
        Contrato contratoTreinador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);
        Treinador treinadorTime = new Treinador(nomeCompletoTreinador, nomeReduzidoTreinador, dataNascimento, contratoTreinador);

        Time novoTime = new Time(nomeTime, nomePopularTime, estadioTime, municipioTime, estadoTime, treinadorTime);
        campeonatoApp.cadastrarTime(novoTime);

        //Flamengo
        nomeTime = "Flamengo";
        //TODO : Verificacao se o time ja existe
        nomePopularTime = "Mengo";
        municipioTime = "Rio de Janeiro";
        estadoTime = "Rio de Janeiro";
        //Estádio
        nomeEstadioTime = "Gavea";
        nomePopularEstadioTime = "Gavea";
        municipioEstadioTime = "Rio de Janeiro";
        estadoEstadioTime = "Rio de Janeiro";
        capacicidadeEstadio = 100;
        estadioTime = new Estadio(nomeEstadioTime, nomePopularEstadioTime, municipioEstadioTime, estadoEstadioTime, capacicidadeEstadio);
        //Treinador
        nomeCompletoTreinador = "Jaime de Almeida";
        nomeReduzidoTreinador = "Jaime";
        dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
        //Contrato - treinador
        valorMultaRecisoria = 100.00;
        valorSalarioBase = 200.00;
        valorDireitosImagem = 300.00;
        dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        dataTermimo = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        descricao = "descricao";
        //Pessoa envolvida - treinador
        nomePessoaEnvolvida = "Pessoa envolvida";
        nomeReduzidoPessoaEnvolvida = "Pessoa envolvida reduzido";
        dataNascimentoPessoaEnvolvida = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");

        pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
        contratoTreinador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);
        treinadorTime = new Treinador(nomeCompletoTreinador, nomeReduzidoTreinador, dataNascimento, contratoTreinador);

        novoTime = new Time(nomeTime, nomePopularTime, estadioTime, municipioTime, estadoTime, treinadorTime);
        campeonatoApp.cadastrarTime(novoTime);

        //Palmeiras
        nomeTime = "Palmeiras";
        //TODO : Verificacao se o time ja existe
        nomePopularTime = "Verdao";
        municipioTime = "Sao Paulo";
        estadoTime = "Sao Paulo";
        //Estádio
        nomeEstadioTime = "Allianz Parque";
        nomePopularEstadioTime = "Parque";
        municipioEstadioTime = "Sao Paulo";
        estadoEstadioTime = "Sao Paulo";
        capacicidadeEstadio = 100;
        estadioTime = new Estadio(nomeEstadioTime, nomePopularEstadioTime, municipioEstadioTime, estadoEstadioTime, capacicidadeEstadio);
        //Treinador
        nomeCompletoTreinador = "Gilson Kleina";
        nomeReduzidoTreinador = "Gilson";
        dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
        //Contrato - treinador
        valorMultaRecisoria = 100.00;
        valorSalarioBase = 200.00;
        valorDireitosImagem = 300.00;
        dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        dataTermimo = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        descricao = "descricao";
        //Pessoa envolvida - treinador
        nomePessoaEnvolvida = "Pessoa envolvida";
        nomeReduzidoPessoaEnvolvida = "Pessoa envolvida reduzido";
        dataNascimentoPessoaEnvolvida = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");

        pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
        contratoTreinador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);
        treinadorTime = new Treinador(nomeCompletoTreinador, nomeReduzidoTreinador, dataNascimento, contratoTreinador);

        novoTime = new Time(nomeTime, nomePopularTime, estadioTime, municipioTime, estadoTime, treinadorTime);
        campeonatoApp.cadastrarTime(novoTime);

        //Corinthians
        nomeTime = "Corinthians";
        //TODO : Verificacao se o time ja existe
        nomePopularTime = "Timao";
        municipioTime = "Sao Paulo";
        estadoTime = "Sao Paulo";
        //Estádio
        nomeEstadioTime = "Arena Corinthians";
        nomePopularEstadioTime = "Arena";
        municipioEstadioTime = "Sao Paulo";
        estadoEstadioTime = "Sao Paulo";
        capacicidadeEstadio = 100;
        estadioTime = new Estadio(nomeEstadioTime, nomePopularEstadioTime, municipioEstadioTime, estadoEstadioTime, capacicidadeEstadio);
        //Treinador
        nomeCompletoTreinador = "Tite";
        nomeReduzidoTreinador = "Tite";
        dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
        //Contrato - treinador
        valorMultaRecisoria = 100.00;
        valorSalarioBase = 200.00;
        valorDireitosImagem = 300.00;
        dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        dataTermimo = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");
        descricao = "descricao";
        //Pessoa envolvida - treinador
        nomePessoaEnvolvida = "Pessoa envolvida";
        nomeReduzidoPessoaEnvolvida = "Pessoa envolvida reduzido";
        dataNascimentoPessoaEnvolvida = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2012");

        pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
        contratoTreinador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);
        treinadorTime = new Treinador(nomeCompletoTreinador, nomeReduzidoTreinador, dataNascimento, contratoTreinador);

        novoTime = new Time(nomeTime, nomePopularTime, estadioTime, municipioTime, estadoTime, treinadorTime);
        campeonatoApp.cadastrarTime(novoTime);
        // fim cadastro estatico de time

        // Cadastro estatico de jogador
        String titulo = "Cadastrar Jogador";
        String timesdisponiveis = "";
        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            timesdisponiveis = timesdisponiveis + "\n" + time.getNomeCompleto();
        }
        nomeTime = "Sao Paulo";

        Time timeJogador = null;

        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            if (time.getNomeCompleto().equals(nomeTime)) {
                timeJogador = time;
            }
        }

        if (timeJogador != null) {
            String nomeJogador = "Jogador";
            String nomePopularJogador = "Jogador";
            int numeroJogador = 10;

            /*
             do {
             numeroJogador = leInt("Numero do jogador", titulo);
             if (timeJogador.getJogadores().containsKey(numeroJogador)) {
             mostraMensagem("O numero " + numeroJogador + " ja pertence a um outro jogador do "
             + timeJogador.getNomePopular() + ", por favor escolha outro numero." , titulo);
             }
                
             } while(timeJogador.getJogadores().containsKey(numeroJogador));
             */
            Date dataNascimentoJogador = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
            Posicao posicaoJogador = null;
            int numPosicao = 13;
            do {



                Posicao[] posicoesPossiveis = Posicao.values();
                for (Posicao posicao : posicoesPossiveis) {
                    if (posicao.getNumeroPosicao() == numPosicao) {
                        posicaoJogador = posicao;
                    }
                }
                if (posicaoJogador == null) {
                    mostraMensagem("Informe um numero de posicao válido!", titulo);
                }
            } while (posicaoJogador == null);

            valorMultaRecisoria = 200.00;
            valorSalarioBase = 200.00;
            valorDireitosImagem = 200.00;
            dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
            dataTermimo = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
            descricao = "descricao";
            //Pessoa envolvida - treinador
            nomePessoaEnvolvida = "pessoa";
            nomeReduzidoPessoaEnvolvida = "pessoa reduzido";
            dataNascimentoPessoaEnvolvida = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");

            pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
            Contrato contratoJogador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);

            Jogador novoJogador = new Jogador(nomeJogador, nomePopularJogador, dataNascimentoJogador, posicaoJogador, contratoJogador);
            timeJogador.cadastrarJogador(numeroJogador, novoJogador);
        }

        //fim cadastro estatico jogador
        
        // Cadastro estatico de jogador
         titulo = "Cadastrar Jogador";
         timesdisponiveis = "";
        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            timesdisponiveis = timesdisponiveis + "\n" + time.getNomeCompleto();
        }
        nomeTime = "Flamengo";

        timeJogador = null;

        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            if (time.getNomeCompleto().equals(nomeTime)) {
                timeJogador = time;
            }
        }

        if (timeJogador != null) {
            String nomeJogador = "Jogador";
            String nomePopularJogador = "Jogador";
            int numeroJogador = 10;

            /*
             do {
             numeroJogador = leInt("Numero do jogador", titulo);
             if (timeJogador.getJogadores().containsKey(numeroJogador)) {
             mostraMensagem("O numero " + numeroJogador + " ja pertence a um outro jogador do "
             + timeJogador.getNomePopular() + ", por favor escolha outro numero." , titulo);
             }
                
             } while(timeJogador.getJogadores().containsKey(numeroJogador));
             */
            Date dataNascimentoJogador = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
            Posicao posicaoJogador = null;
            int numPosicao = 13;
            do {



                Posicao[] posicoesPossiveis = Posicao.values();
                for (Posicao posicao : posicoesPossiveis) {
                    if (posicao.getNumeroPosicao() == numPosicao) {
                        posicaoJogador = posicao;
                    }
                }
                if (posicaoJogador == null) {
                    mostraMensagem("Informe um numero de posicao válido!", titulo);
                }
            } while (posicaoJogador == null);

            valorMultaRecisoria = 200.00;
            valorSalarioBase = 200.00;
            valorDireitosImagem = 200.00;
            dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
            dataTermimo = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");
            descricao = "descricao";
            //Pessoa envolvida - treinador
            nomePessoaEnvolvida = "pessoa";
            nomeReduzidoPessoaEnvolvida = "pessoa reduzido";
            dataNascimentoPessoaEnvolvida = new SimpleDateFormat("dd/MM/yyyy").parse("30/11/1955");

            pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
            Contrato contratoJogador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);

            Jogador novoJogador = new Jogador(nomeJogador, nomePopularJogador, dataNascimentoJogador, posicaoJogador, contratoJogador);
            timeJogador.cadastrarJogador(numeroJogador, novoJogador);
        }

        //fim cadastro estatico jogador        

        
         int opcao;
        
         do {
         try {
         opcao = CampeonatoApp.leInt("Escolha umas das opções abaixo:\n"
         + "1 - Cadastrar time\n2 - Cadastrar jogador em um time\n"
         + "3 - Gerar rodadas\n"
         + "4 - Cadastrar resultado rodadas\n5 - Visualizar Classificação"
         + "\n6 - Sair", "Menu de opções");

         } catch (NumberFormatException e) {
         opcao = Integer.MIN_VALUE;

         }

         try {

         switch (opcao) {
         case Integer.MIN_VALUE:
         CampeonatoApp.mostraMensagem("Número digitado no formato incorreto!", "Exceção");
         break;

         default:
         CampeonatoApp.mostraMensagem("Opção " + opcao
         + " não pertence ao menu!\n Você deve escolher uma opção de menu válida.",
         "Opção inválida!");

         break;
         case 1:
         CampeonatoApp.cadastrarTime(campeonatoApp);

         break;

         case 2:
         CampeonatoApp.cadastrarJogadoresTime(campeonatoApp);

         break;
         case 3:

         CampeonatoApp.gerarRodadas(campeonatoApp);
         break;

         case 4:
         CampeonatoApp.cadastrarResultadoRodada(campeonatoApp);
         break;

         case 5:
         CampeonatoApp.imprimirClassificacaoAtualizada(campeonatoApp);
         break;

         case 6:
         CampeonatoApp.mostraMensagem("Sistema finalizado!", campeonatoApp.getNomeCampeonato());
         break;
         case Integer.MAX_VALUE:
         ;                        

         }
         } catch (Exception e) {
         //Tratar a exceção que foi lançada por alguém

         CampeonatoApp.mostraMensagem(e.getMessage(), "Exceção");

         }

         } while (opcao != 6);        
        
    }

    private static void cadastrarTime(Campeonato campeonatoApp) throws Exception {
        String nomeTime = CampeonatoApp.leString("Informe o nome do time", "Cadastro Time");

        //verificar se o time ja existe
        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            if (time.getNomeCompleto().equals(nomeTime)) {
                throw new Exception("O time informado ja existe!");
            }
        }

        String nomePopularTime = CampeonatoApp.leString("Informe o nome popular do time", "Cadastro Time");
        String municipioTime = CampeonatoApp.leString("Informe o municipio do time", "Cadastro Time");
        String estadoTime = CampeonatoApp.leString("Informe o Estado de localização do time", "Cadastro time");
        //Estádio
        String nomeEstadioTime = CampeonatoApp.leString("Informe o nome do Estádio do time", "Cadastro Time");
        String nomePopularEstadioTime = CampeonatoApp.leString("Informe o nome popular do Estádio do time", "Cadastro Time");
        String municipioEstadioTime = CampeonatoApp.leString("Informe o município de localização do Estádio do time", "Cadastro Time");
        String estadoEstadioTime = CampeonatoApp.leString("Informe Estado de localização do Estádio do time", "Cadastro Time");
        int capacicidadeEstadio = CampeonatoApp.leInt("Informe a capacidade máxima do Estádio do time", "Cadastro Time");
        Estadio estadioTime = new Estadio(nomeEstadioTime, nomePopularEstadioTime, municipioEstadioTime, estadoEstadioTime, capacicidadeEstadio);
        //Treinador
        String nomeCompletoTreinador = CampeonatoApp.leString("Informe o nome completo do treinador", "Cadastro Time");
        String nomeReduzidoTreinador = CampeonatoApp.leString("Informe o nome reduzido do treinador", "Cadastro Time");
        Date dataNascimento = CampeonatoApp.leData("Informe a data de nascimento do treinador (no formato dd/mm/aaaa)", "Cadatro Time");
        //Contrato - treinador
        Double valorMultaRecisoria = leDouble("Informe o valor da multa recisória", "Cadastro Time - Contrato Treinador");
        Double valorSalarioBase = leDouble("Informe o valor do salário base", "Cadastro Time - Contrato Treinador");
        Double valorDireitosImagem = leDouble("Informe o valor dos direitos de imagem", "Cadastro Time - Contrato Treinador");
        Date dataInicio = leData("Data de inicio de contrato", "Cadastro Time - Contrato Treinador");
        Date dataTermimo = leData("Data de término do contrato", "Cadastro Time - Contrato Treinador");
        String descricao = leString("Descrição do contrato", "Cadastro Time - Contrato Treinador");
        //Pessoa envolvida - treinador
        String nomePessoaEnvolvida = leString("Nome pessoa envolvida", "Cadastro Time - Contrato Treinador");
        String nomeReduzidoPessoaEnvolvida = leString("Nome reduzido pessoa envolvida", "Cadastro Time - Contrato Treinador");
        Date dataNascimentoPessoaEnvolvida = leData("Informe a data de nascimento da pessoa envolvida (no formato dd/mm/aaaa)", "Cadastro Time - Contrato Treinador");

        Pessoa pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
        Contrato contratoTreinador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);
        Treinador treinadorTime = new Treinador(nomeCompletoTreinador, nomeReduzidoTreinador, dataNascimento, contratoTreinador);

        Time novoTime = new Time(nomeTime, nomePopularTime, estadioTime, municipioTime, estadoTime, treinadorTime);
        campeonatoApp.cadastrarTime(novoTime);

    }

    private static void cadastrarJogadoresTime(Campeonato campeonatoApp) throws Exception {
        String titulo = "Cadastrar Jogador";
        String timesdisponiveis = "";
        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            timesdisponiveis = timesdisponiveis + "\n" + time.getNomeCompleto();
        }
        String nomeTime = leString("times disponiveis:\n" + timesdisponiveis + "\n\nNome do time", titulo);

        Time timeJogador = null;

        for (Time time : campeonatoApp.getTimesDoCampeonato()) {
            if (time.getNomeCompleto().equals(nomeTime)) {
                timeJogador = time;
            }
        }

        if (timeJogador != null) {
            String nomeJogador = leString("Nome do jogador", titulo);
            String nomePopularJogador = leString("Nome Popular do jogador", titulo);
            int numeroJogador;
            do {
                numeroJogador = leInt("Numero do jogador", titulo);
                if (timeJogador.getJogadores().containsKey(numeroJogador)) {
                    mostraMensagem("O numero " + numeroJogador + " ja pertence a um outro jogador do "
                            + timeJogador.getNomePopular() + ", por favor escolha outro numero.", titulo);
                }

            } while (timeJogador.getJogadores().containsKey(numeroJogador));

            Date dataNascimentoJogador = leData("Informe a data de nascimento do jogador (no formato dd/mm/aaaa)", titulo);
            Posicao posicaoJogador = null;
            do {
                int numPosicao = leInt("Informe a posição do jogador\n1 - Goleiro\n2 - Zagueiro Direito"
                        + "\n3 - Zagueiro Esquerdo\n4 - Zagueiro\n5 - Lateral Direito \n6 - Lateral\n7 - Lateral Esquerdo"
                        + "\n8 - Meio Campo\n9 - Volante\n10 - Armador\n11 - Ala Direito\n12 - Ala Esquerdo"
                        + "\n13 - Atacante\n14 - Atacante Esquerdo\n15 - Atacante Direito\n16 - Centro Avante", titulo);


                Posicao[] posicoesPossiveis = Posicao.values();
                for (Posicao posicao : posicoesPossiveis) {
                    if (posicao.getNumeroPosicao() == numPosicao) {
                        posicaoJogador = posicao;
                    }
                }
                if (posicaoJogador == null) {
                    mostraMensagem("Informe um numero de posicao válido!", titulo);
                }
            } while (posicaoJogador == null);

            Double valorMultaRecisoria = leDouble("Informe o valor da multa recisória", titulo);
            Double valorSalarioBase = leDouble("Informe o valor do salário base", titulo);
            Double valorDireitosImagem = leDouble("Informe o valor dos direitos de imagem", titulo);
            Date dataInicio = leData("Data de inicio de contrato (no formato dd/mm/aaaa)", titulo);
            Date dataTermimo = leData("Data de término do contrato (no formato dd/mm/aaaa)", titulo);
            String descricao = leString("Descrição do contrato", titulo);
            //Pessoa envolvida - treinador
            String nomePessoaEnvolvida = leString("Nome pessoa envolvida", titulo);
            String nomeReduzidoPessoaEnvolvida = leString("Nome reduzido pessoa envolvida", titulo);
            Date dataNascimentoPessoaEnvolvida = leData("Informe a data de nascimento da pessoa envolvida (no formato dd/mm/aaaa)", titulo);

            Pessoa pessoaEnvolvida = new Pessoa(nomePessoaEnvolvida, nomeReduzidoPessoaEnvolvida, dataNascimentoPessoaEnvolvida);
            Contrato contratoJogador = new Contrato(valorMultaRecisoria, valorSalarioBase, valorDireitosImagem, dataInicio, dataTermimo, descricao, pessoaEnvolvida);

            Jogador novoJogador = new Jogador(nomeJogador, nomePopularJogador, dataNascimentoJogador, posicaoJogador, contratoJogador);
            timeJogador.cadastrarJogador(numeroJogador, novoJogador);
        }
        else {
            throw new Exception("O time informado não existe!");
        }

    }

    private static void gerarRodadas(Campeonato campeonatoApp) throws Exception {
        campeonatoApp.inicializarRodadas();
    }

    private static void cadastrarResultadoRodada(Campeonato campeonatoApp) throws Exception {
        String titulo = "Cadastrar informaçoes rodada";
        int numeroRodada = leInt("Numero da rodada", null);
        Rodada buscaRodada = null;
        for (Rodada rodada : campeonatoApp.getRodadasDoCampeonato()) {
            if (rodada.getNumeroDaRodada() == numeroRodada) {
                buscaRodada = rodada;
            }
        }

        if (buscaRodada != null) {
            String partidasEncontradas = "";
            for (Partida partida : buscaRodada.getPartidas()) {
                partidasEncontradas += partida.getTimeDaCasa().getNomeCompleto() + " vs " + partida.getTimeVisitante().getNomeCompleto() + "\n";
            }
            mostraMensagem("Partidas:\n\n" + partidasEncontradas, titulo);
            //cadastro de gol
            for (Partida partida : buscaRodada.getPartidas()) {
                if(partida.getTimeDaCasa().getJogadores().isEmpty() 
                        || partida.getTimeVisitante().getJogadores().isEmpty())
                    throw new Exception("Cadastre jogadores antes de cadastrar informacoes para a partida: \n\n"
                    + partida.getTimeDaCasa().getNomeCompleto() + " vs " + partida.getTimeVisitante().getNomeCompleto());
                Integer qtdGols = leInt("Gols marcados pelo " + partida.getTimeDaCasa().getNomeCompleto(), titulo);
                for (int i = 0; i < qtdGols; i++) {
                    String nomeJogadorBusca = leString("Nome do jogador autor do gol " + (i+1), titulo);
                    Jogador jogador = null;
                    for (Map.Entry<Integer, Jogador> entry : partida.getTimeDaCasa().getJogadores().entrySet()) {
                        Jogador objJogador = entry.getValue();
                        String nomeJogador = objJogador.getNomeCompleto();
                        if(nomeJogador.equals(nomeJogadorBusca))
                            jogador = objJogador;
                    }
                    if(jogador != null) {
                        Integer tempo = leInt("Tempo em que o gol foi marcado(1 ou 2)", titulo);
                        String minutos = leString("Minutos em que o gol foi marcado", titulo);
                        Gol golTimeDaCasa = new Gol(null, minutos, numeroRodada);
                        partida.cadastrarGolTimeCasa(golTimeDaCasa);
                    }
                    else {
                        throw new Exception("jogador nao encontrado!");
                    }
                }
                qtdGols = leInt("Gols marcados pelo " + partida.getTimeVisitante().getNomeCompleto(), titulo);
                for (int i = 0; i < qtdGols; i++) {
                    String nomeJogadorBusca = leString("Nome do jogador autor do gol " + (i+1), titulo);
                    Jogador jogador = null;
                    for (Map.Entry<Integer, Jogador> entry : partida.getTimeVisitante().getJogadores().entrySet()) {
                        Jogador objJogador = entry.getValue();
                        String nomeJogador = objJogador.getNomeCompleto();
                        if(nomeJogador.equals(nomeJogadorBusca))
                            jogador = objJogador;
                    }
                    if(jogador != null) {
                        Integer tempo = leInt("Tempo em que o gol foi marcado(1 ou 2)", titulo);
                        String minutos = leString("Minutos em que o gol foi marcado", titulo);
                        Gol golTimeVisitante = new Gol(null, minutos, numeroRodada);
                        partida.cadastrarGolTimeCasa(golTimeVisitante);
                    }
                    else {
                        throw new Exception("jogador nao encontrado!");
                    }
                }
                mostraMensagem("Gols da partida " + partida.getTimeDaCasa().getNomeCompleto() 
                        + " vs " + partida.getTimeVisitante().getNomeCompleto() + " cadastrados com sucesso!", titulo);
            }
            
            
            
        }
        else {
            throw new Exception("Rodada nao encontrada!!\nTente gerar as rodadas utilizando a opçao 3 do menu.");
        }
    }

    private static void imprimirClassificacaoAtualizada(Campeonato campeonatoApp) throws Exception {
        String titulo = "Classificao Atualizada";
        HashMap<Integer, ColocacaoTabela> HashTabela;
        HashTabela = campeonatoApp.tabelaClassificacao();
        if (HashTabela.isEmpty()) {
            mostraMensagem("Nao há classificacoes! (Provavelmente nao há rodadas geradas)", titulo);
        }
        else {
            String tabelaString = "";
            for (Map.Entry<Integer, ColocacaoTabela> entry : HashTabela.entrySet()) {
                Integer integer = entry.getKey();
                ColocacaoTabela colocacaoTabela = entry.getValue();
                tabelaString = tabelaString + "\n" + integer + " \t " + colocacaoTabela.getTime().getNomeCompleto()
                        + " \t\t " + colocacaoTabela.getnPontos() + " ponto" + (colocacaoTabela.getnPontos() > 1? "s" : "");
            }
            mostraMensagem(tabelaString, titulo);
        }
    }

    private static Date leData(String texto, String titulo) throws ParseException {
        String entrada = (String) JOptionPane.showInputDialog(null,
                texto, titulo,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        Date data = new SimpleDateFormat("dd/MM/yyyy").parse(entrada);

        return data;
    }

    public static int leInt(String texto, String titulo) {

        String entrada = (String) JOptionPane.showInputDialog(null,
                texto, titulo,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        if (entrada == null) {
            return Integer.MAX_VALUE;
        }


        return Integer.parseInt(entrada);

    }

    public static double leDouble(String texto, String titulo) {
        String entrada = (String) JOptionPane.showInputDialog(null,
                texto, titulo,
                JOptionPane.QUESTION_MESSAGE, null, null, null);
        return Double.parseDouble(entrada);
    }

    public static String leString(String texto, String titulo) {
        String entrada = (String) JOptionPane.showInputDialog(null,
                texto, titulo,
                JOptionPane.QUESTION_MESSAGE, null, null, null);
        return entrada;
    }

    public static void mostraMensagem(String texto, String titulo) {
        JOptionPane.showMessageDialog(null, texto, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
}