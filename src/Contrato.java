
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class Contrato {
    private Double valorMultaRescisoria,valorSalarioBase,valorDireitosDeImagem;
    private Date dataInicio, dataTermino;
    private String descricao;
    private Pessoa pessoaEnvolvida;
    private Time timeEnvolvido; 

    public Contrato(Double valorMultaRescisoria, Double valorSalarioBase, Double valorDireitosDeImagem, Date dataInicio, Date dataTermino, String descricao, Pessoa pessoaEnvolvida) {
        this.valorMultaRescisoria = valorMultaRescisoria;
        this.valorSalarioBase = valorSalarioBase;
        this.valorDireitosDeImagem = valorDireitosDeImagem;
        this.dataInicio = dataInicio;
        this.dataTermino = dataTermino;
        this.descricao = descricao;
        this.pessoaEnvolvida = pessoaEnvolvida;
    }
    
    
    public Double getValorMultaRescisoria() {
        return valorMultaRescisoria;
    }

    public void setValorMultaRescisoria(Double valorMultaRescisoria) {
        this.valorMultaRescisoria = valorMultaRescisoria;
    }

    public Double getValorSalarioBase() {
        return valorSalarioBase;
    }

    public void setValorSalarioBase(Double valorSalarioBase) {
        this.valorSalarioBase = valorSalarioBase;
    }

    public Double getValorDireitosDeImagem() {
        return valorDireitosDeImagem;
    }

    public void setValorDireitosDeImagem(Double valorDireitosDeImagem) {
        this.valorDireitosDeImagem = valorDireitosDeImagem;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(Date dataTermino) {
        this.dataTermino = dataTermino;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Pessoa getPessoaEnvolvida() {
        return pessoaEnvolvida;
    }

    public void setPessoaEnvolvida(Pessoa pessoaEnvolvida) {
        this.pessoaEnvolvida = pessoaEnvolvida;
    }

    public Time getTimeEnvolvido() {
        return timeEnvolvido;
    }

    public void setTimeEnvolvido(Time timeEnvolvido) {
        this.timeEnvolvido = timeEnvolvido;
    }

    @Override
    public String toString() {
        return "Contrato{" + "valorMultaRescisoria=" + valorMultaRescisoria + ", valorSalarioBase=" + valorSalarioBase + ", valorDireitosDeImagem=" + valorDireitosDeImagem + ", dataInicio=" + dataInicio + ", dataTermino=" + dataTermino + ", descricao=" + descricao + ", pessoaEnvolvida=" + pessoaEnvolvida + ", timeEnvolvido=" + timeEnvolvido + '}';
    }
       
}
