
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public class PessoaComContrato extends Pessoa {
    protected Contrato contratoVigente;

    public PessoaComContrato(String nomeCompleto, String nomeReduzidoPopular, Date dataNascimento, Contrato contrato) {
        super(nomeCompleto, nomeReduzidoPopular, dataNascimento);
        this.contratoVigente = contrato;
    }
    
    public Contrato getContratoVigente() {
        return contratoVigente;
    }

    public void setContratoVigente(Contrato contratoVigente) {
        this.contratoVigente = contratoVigente;
    }

    @Override
    public String toString() {
        return "PessoaComContrato{" + "contratoVigente=" + contratoVigente + '}';
    }
    
}
