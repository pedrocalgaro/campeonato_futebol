/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hercules Sandim
 */
public enum Posicao {
    GOLEIRO("Goleiro", 1),
    ZAGUEIRO_DIREITO("Zagueiro Direito", 2),
    ZAGUEIRO("Zagueiro", 3),
    ZAGUEIRO_ESQUERDO("Zagueiro Esquerdo", 4),
    LATERAL_DIREITO("Lateral Direito", 5),
    LATERAL("Lateral", 6),
    LATERAL_ESQUERDO("Lateral Esquerdo", 7),
    MEIO_CAMPO("Meio Campo", 8),
    VOLANTE("Volante", 9),
    ARMADOR("Armador", 10),
    ALA_DIREITO("Ala Direiro", 11),
    ALA_ESQUERDO("Ala esquerdo", 12),
    ATACANTE("Atacante", 13),
    ATACANTE_ESQUERDO("Atacante Esquerdo", 14),
    ATACANTE_DIREITO("Atacante Direito", 15),
    CENTRO_AVANTE("Centro Avante", 16);
    
    public int numeroPosicao;
    public String nome;
    private Posicao(String nome, int tipoJogador) {
        this.nome = nome;
        this.numeroPosicao = tipoJogador;
    }

    public int getNumeroPosicao() {
        return numeroPosicao;
    }

    public String getNome() {
        return nome;
    }
    
    
}
